<?php # Script 9.6 - view_users.php #2
// This script retrieves all the records from the users table.
echo "<body style='background-color:618685'>";


$page_title = 'Our Technicians';
include('includes/header.html');

// Page header:
echo '<h1>Our Technicians</h1>';

require('mysqli_connect.php'); // Connect to the db.

// Make the query:
$q = "SELECT techID, firstName, LastName, email, phone from technicians WHERE 1;";
//$q = "SELECT customerID AS id, CONCAT(firstName, ', ', lastName) AS name,state, email FROM customers WHERE 1";
$r = @mysqli_query($dbc, $q); // Run the query.

// Count the number of returned rows:
$num = mysqli_num_rows($r);

if ($num > 0) { // If it ran OK, display the records.

	// Print how many users there are:
	echo "<p>There are currently $num registered users.</p>\n";

	// Table header.
	echo '<table class="table">
	<thead>
	<tr>
		<th align="left">Last Name</th>
		<th align="left">First Name</th>
		<th align="left">Email</th>
		<th align="left">Phone</th>
		<th align="left">Update</th>
	</tr>
	</thead>
	<tbody>
';

	// Fetch and print all the records:
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		$number = $row['id'];
		echo '<tr>
		<td align="left">' . $row['lastName'] . '</td>
		<td align="left">' . $row['firstName'] . '</td>
		<td align="left">' . $row['email'] . '</td>
		<td align="left">' . $row['phone'] . '</td>
		<td align="left"><b><a href="view_incidents.php?thing='. $row['techID'] .'">View Customer Incidents</a></b></td></tr>
		';
	}

	echo '</tbody></table>'; // Close the table.

	mysqli_free_result ($r); // Free up the resources.

} else { // If no records were returned.

	echo '<p class="error">There are currently no Technicians.</p>';

}


mysqli_close($dbc); // Close the database connection.

include('includes/footer.html');
?>