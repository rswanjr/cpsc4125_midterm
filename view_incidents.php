<?php # Script 9.6 - view_users.php #2
// This script retrieves all the records from the users table.
echo "<body style='background-color:618685'>";

$thing = (isset($_GET['thing'])) ?
$_GET['thing'] : 'rd';



$page_title = 'Incidents';
include('includes/header.html');

// Page header:
echo '<h1>Open Incidents for for Customer</h1>';

require('mysqli_connect.php'); // Connect to the db.

// Make the query:
//SELECT `incidentID`, `productCode`, `techID`, `title` FROM `incidents` WHERE `customerID` = 1010; 
$q ="SELECT incidentID, productCode, techID, title FROM incidents WHERE customerID = '$thing'";
//$q ="SELECT incidentID AS id, title, productCode AS pc, FROM incidents WHERE customerID = '$thing'";

$r = @mysqli_query($dbc, $q); // Run the query.

// Count the number of returned rows:
$num = mysqli_num_rows($r);

if ($num > 0) { // If it ran OK, display the records.

	// Print how many users there are:
	echo "<p>There are currently $num incident post.</p>\n";

	// Table header.
	echo '<table class="table table-striped" width="70%" style = "background-color: C0C0C0">
	<thead class="thead-dark"f
	<tr>
		<th align="left">Incident ID</th>
		<th align="left">Title</th>
		<th align="left">Product Code</th>
		<th align="left">Tech Name</th>
	</tr>
	</thead>
	<tbody>
';

	// Fetch and print all the records:
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		echo '<tr><td align="left" >' . $row['incidentID'] . '</td> 
		<td align="left">' . $row['title'] . '</td> 
		<td align="left">' . $row['productCode'] .'</td>
		<td align="left">' . $row['techID'] .'</td>
		</tr>
		';
	}

	echo '</tbody></table>'; // Close the table.

	mysqli_free_result ($r); // Free up the resources.

} else { // If no records were returned.

	echo '<p class="error">This customer has no incidents.</p>';

}

mysqli_close($dbc); // Close the database connection.

include('includes/footer.html');
?>