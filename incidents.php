<?php # Script 9.6 - view_users.php #2
// This script retrieves all the records from the users table.
echo "<body style='background-color:618685'>";


$page_title = 'View the Current Incidents';
include('includes/header.html');

// Page header:
echo '<h1>All Open Incidents</h1>';

require('mysqli_connect.php'); // Connect to the db.

// Make the query:
$q = "SELECT incidentID, dateOpened,productCode,techID, title FROM incidents WHERE 1";
//$q = "SELECT customerID AS id, CONCAT(firstName, ', ', lastName) AS name,state, email FROM customers WHERE 1";
$r = @mysqli_query($dbc, $q); // Run the query.

// Count the number of returned rows:
$num = mysqli_num_rows($r);

if ($num > 0) { // If it ran OK, display the records.

	// Print how many users there are:
	echo "<p>There are currently $num registered incidents.</p>\n";

	// Table header.
	echo '<table class="table">
	<thead>
	<tr>
		<th align="left">Tech Name</th>
		<th align="left">Incident ID</th>
		<th align="left">Title</th>
		<th align="left">Product Code</th>
		<th align="left">Date Opened</th>
	</tr>
	</thead>
	<tbody>
';

	// Fetch and print all the records:
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		$number = $row['id'];
		echo '<tr>
		<td align="left">' . $row['techID'] . '</td>
		<td align="left">' . $row['incidentID'] . '</td>
		<td align="left">' . $row['title'] . '</td>
		<td align="left">' . $row['productCode'] . '</td>
		<td align="left">' . $row['dateOpened'] . '</td>
		';
	}

	echo '</tbody></table>'; // Close the table.

	mysqli_free_result ($r); // Free up the resources.

} else { // If no records were returned.

	echo '<p class="error">There are currently no Customers.</p>';

}


mysqli_close($dbc); // Close the database connection.

include('includes/footer.html');
?>