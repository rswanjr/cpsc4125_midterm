<?php # Script 9.6 - view_users.php #2
// This script retrieves all the records from the users table.
echo "<body style='background-color:618685'>";


$page_title = 'View the Current Customers';
include('includes/header.html');

// Page header:
echo '<h1>Our Customers</h1>';

require('mysqli_connect.php'); // Connect to the db.

// Make the query:
//$q = "SELECT CONCAT(last_name, ', ', first_name) AS name, username AS un FROM users ORDER BY last_name ASC";
//$q = "SELECT username AS un, user_id AS id FROM users ORDER BY user_id ASC";
$q = "SELECT customerID AS id, CONCAT(firstName, ', ', lastName) AS name,state, email FROM customers WHERE 1";
$r = @mysqli_query($dbc, $q); // Run the query.

// Count the number of returned rows:
$num = mysqli_num_rows($r);

if ($num > 0) { // If it ran OK, display the records.

	// Print how many users there are:
	echo "<p>There are currently $num registered users.</p>\n";

	// Table header.
	echo '<table class="table">
	<thead>
	<tr>
		<th align="left">State</th>
		<th align="left">Name</th>
		<th align="left">Email</th>
		<th align="left">Customer Incidents</th>
		<th align="left">Customer Registrations</th>
	</tr>
	</thead>
	<tbody>
';

	// Fetch and print all the records:
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		$number = $row['id'];
		echo '<tr>
		<td align="left">' . $row['state'] . '</td>
		<td align="left">' . $row['name'] . '</td>
		<td align="left">' . $row['email'] . '</td>
		<td align="left"><b><a href="view_incidents.php?thing='. $row['id'] .'">View Customer Incidents</a></b></td></tr>
		<td align="left"><b><a href="view_customerP.php?thing='. $row['id'] .'">View Product by Customer</a></b></td></tr>
		';
	}

	echo '</tbody></table>'; // Close the table.

	mysqli_free_result ($r); // Free up the resources.

} else { // If no records were returned.

	echo '<p class="error">There are currently no Customers.</p>';

}


mysqli_close($dbc); // Close the database connection.

include('includes/footer.html');
?>